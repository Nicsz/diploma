<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{

//	public $timestamps = false;

	protected $table = 'tournaments';

	protected $fillable = [
		'league_img_url', 'league_name', 'match_name', 'match_status', 'prizepool', 'serie_full_name', 'year',
	];

	public $timestamps = false;

	public function teams()
	{
		return $this->belongsToMany('App\Team');
	}
}