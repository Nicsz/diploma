<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
	protected $table = 'teams';

	protected $fillable = ['image', 'team_name'];

	public function tournaments()
	{
		return $this->belongsToMany('App\Tournament');
	}

}
