<?php

namespace App\Http\Controllers;

use App\Tournament;
use Illuminate\Http\Request;

class IndexController extends Controller
{
	public function index() {

		$tournaments = Tournament::all();
		return view('index', compact('tournaments'));
	}
}
