<?php

namespace App\Http\Controllers;

use App\Tournament;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class ApiRequestController extends Controller
{
	public function getRequestR6TabApi() {

		$client = new Client(['headers' => ['content-type' => 'application/json', 'Accept' => 'application/json']]);

//		$params = array('tournaments', 'teams');

//		dd($params[1]);

//		foreach ($params as $param) {
//			$header = get_headers("https://api.pandascore.co/csgo/tournaments?range[begin_at]=2018-01-01T17:00:00Z,2018-12-31T22:00:00Z&token=EYglI_LuOdfZ8rCzfYSn2_Z5C14GRPKIF9jl-vYjaLy-WB4XGJg");
			$header = get_headers("https://api.pandascore.co/csgo/teams?token=EYglI_LuOdfZ8rCzfYSn2_Z5C14GRPKIF9jl-vYjaLy-WB4XGJg");
//		dd($header);
			$str = preg_replace("/[^0-9]/", '', $header[15]);
//		dd(ceil($str / 50));
			for ($page = 1; $page <= ceil($str / 50); $page++) {
//				if ($param == 'teams') {
					$response = $client->request('GET', "https://api.pandascore.co/csgo/teams", [
						'query' => [
							'page' => $page,
							'token' => 'EYglI_LuOdfZ8rCzfYSn2_Z5C14GRPKIF9jl-vYjaLy-WB4XGJg'
						]
					]);

					$csgo_teams = $response->getBody();
					$csgo_teams = json_decode($csgo_teams);

//					dd($csgo_teams);

					foreach ($csgo_teams as $key => $value) {
						$qry = DB::insert('insert into teams(
						team_id, 
						image, 
						team_name
					)values(?,?,?)',
							[
								$csgo_teams[$key]->id,
								$csgo_teams[$key]->image_url,
								$csgo_teams[$key]->name,
							]
						); //index name will be paper_id,question_no etc

						foreach ($value as $a => $b) {
							if ($a == 'players') {
								for ($i = 0; $i < count($b); $i++) {
									$qry = DB::insert('insert into players(
											first_name,
											hometown,
											player_id,
											image,
											last_name,
											nickname
										)values(?,?,?,?,?,?)',
										[
											$csgo_teams[$key]->$a[$i]->first_name,
											$csgo_teams[$key]->$a[$i]->hometown,
											$csgo_teams[$key]->$a[$i]->id,
											$csgo_teams[$key]->$a[$i]->image_url,
											$csgo_teams[$key]->$a[$i]->last_name,
											$csgo_teams[$key]->$a[$i]->name,
										]
									);
								}
							}
						}
					}
//				}

//				elseif ($param == 'tournaments') {
					/*$response = $client->request('GET', 'https://api.pandascore.co/csgo/tournaments', [
						'query' => [
							'range[begin_at]' => '2018-01-01T17:00:00Z,2018-12-31T22:00:00Z',
							'token' => 'EYglI_LuOdfZ8rCzfYSn2_Z5C14GRPKIF9jl-vYjaLy-WB4XGJg'
						]
					]);

					$csgo_tournaments = $response->getBody();
					$csgo_tournaments = json_decode($csgo_tournaments);

					foreach ($csgo_tournaments as $key => $value) {
						$qry=DB::insert('insert into tournaments(
								tournament_id,
								league_id,
								league_img_url,
								league_name,
								prizepool,
								serie_full_name,
								serie_id,
								tournament_year,
								winner_id,
								begin_at,
								end_at,
								modified_at
							)values(?,?,?,?,?,?,?,?,?,?,?,?)',
							[
								$csgo_tournaments[$key]->id,
								$csgo_tournaments[$key]->league->id,
								$csgo_tournaments[$key]->league->image_url,
								$csgo_tournaments[$key]->league->name,
								$csgo_tournaments[$key]->prizepool,
								$csgo_tournaments[$key]->serie->full_name,
								$csgo_tournaments[$key]->serie->id,
								$csgo_tournaments[$key]->serie->year,
								$csgo_tournaments[$key]->serie->winner_id,
								$csgo_tournaments[$key]->begin_at,
								$csgo_tournaments[$key]->end_at,
								$csgo_tournaments[$key]->modified_at,
							]
						);

						foreach ($value as $a => $b)
						{
							if($a == 'matches') {
								for ($i = 0; $i < count($b); $i++) {

									$qry=DB::insert('insert into matches(
											match_id,
											match_name,
											match_status,
											draw,
											match_winner_id,
											begin_at,
											end_at,
											modified_at
										)values(?,?,?,?,?,?,?,?)',
										[
											$csgo_tournaments[$key]->$a[$i]->id,
											$csgo_tournaments[$key]->$a[$i]->name,
											$csgo_tournaments[$key]->$a[$i]->status,
											$csgo_tournaments[$key]->$a[$i]->draw,
											$csgo_tournaments[$key]->$a[$i]->winner_id,
											$csgo_tournaments[$key]->$a[$i]->begin_at,
											$csgo_tournaments[$key]->$a[$i]->end_at,
											$csgo_tournaments[$key]->$a[$i]->modified_at,
										]
									);
								}
							}
						}
					}*/
				}
//			}


					/*$response = $client->request('GET', 'https://api.pandascore.co/csgo/tournaments', [
						'query' => [
							'range[begin_at]' => '2018-01-01T17:00:00Z,2018-12-31T22:00:00Z',
							'token' => 'EYglI_LuOdfZ8rCzfYSn2_Z5C14GRPKIF9jl-vYjaLy-WB4XGJg'
						]
					]);



					$csgo_tournaments = $response->getBody();
					$csgo_tournaments = json_decode($csgo_tournaments);*/


//		dd($csgo_tournaments);

			/*$response1 = $client->request('GET', 'https://api.pandascore.co/csgo/teams', [
				'query' => [
					'per_page' => 100,
					'token' => 'EYglI_LuOdfZ8rCzfYSn2_Z5C14GRPKIF9jl-vYjaLy-WB4XGJg'
				]
			]);
			$csgo_teams = $response1->getBody();
			$csgo_teams = json_decode($csgo_teams);*/

//		dd($csgo_tournaments[0]);
			/*foreach ($csgo_tournaments as $key => $value)
			{
				$qry=DB::insert('insert into tournaments(
						tournament_id,
						league_id,
						league_img_url,
						league_name,
						prizepool,
						serie_full_name,
						serie_id,
						tournament_year,
						winner_id,
						begin_at,
						end_at,
						modified_at
					)values(?,?,?,?,?,?,?,?,?,?,?,?)',
					[
						$csgo_tournaments[$key]->id,
						$csgo_tournaments[$key]->league->id,
						$csgo_tournaments[$key]->league->image_url,
						$csgo_tournaments[$key]->league->name,
						$csgo_tournaments[$key]->prizepool,
						$csgo_tournaments[$key]->serie->full_name,
						$csgo_tournaments[$key]->serie->id,
						$csgo_tournaments[$key]->serie->year,
						$csgo_tournaments[$key]->serie->winner_id,
						$csgo_tournaments[$key]->begin_at,
						$csgo_tournaments[$key]->end_at,
						$csgo_tournaments[$key]->modified_at,
					]
				); //index name will be paper_id,question_no etc

				foreach ($value as $a => $b)
				{
					if($a == 'matches') {
						/*echo "<pre>";
						print_r($b);
	//					dd(count($csgo_tournaments[$key]->$a));
						for ($i = 0; $i < count($b); $i++) {
	//						dd($csgo_tournaments[$key]->$a[$i+6]);

							$qry=DB::insert('insert into matches(
									match_id,
									match_name,
									match_status,
									draw,
									match_winner_id,
									begin_at,
									end_at,
									modified_at
								)values(?,?,?,?,?,?,?,?)',
								[
									$csgo_tournaments[$key]->$a[$i]->id,
									$csgo_tournaments[$key]->$a[$i]->name,
									$csgo_tournaments[$key]->$a[$i]->status,
									$csgo_tournaments[$key]->$a[$i]->draw,
									$csgo_tournaments[$key]->$a[$i]->winner_id,
									$csgo_tournaments[$key]->$a[$i]->begin_at,
									$csgo_tournaments[$key]->$a[$i]->end_at,
									$csgo_tournaments[$key]->$a[$i]->modified_at,
								]
							); //index name will be paper_id,question_no etc
						}
					}
				}
			}

			foreach ($csgo_teams as $key => $value)
			{
				$qry=DB::insert('insert into teams(
						team_id,
						image,
						team_name
					)values(?,?,?)',
					[
						$csgo_teams[$key]->id,
						$csgo_teams[$key]->image_url,
						$csgo_teams[$key]->name,
					]
				); //index name will be paper_id,question_no etc

				foreach ($value as $a => $b)
				{
					if($a == 'players') {
						for ($i = 0; $i < count($b); $i++) {
							$qry=DB::insert('insert into players(
									first_name,
									hometown,
									player_id,
									image,
									last_name,
									nickname
								)values(?,?,?,?,?,?)',
								[
									$csgo_teams[$key]->$a[$i]->first_name,
									$csgo_teams[$key]->$a[$i]->hometown,
									$csgo_teams[$key]->$a[$i]->id,
									$csgo_teams[$key]->$a[$i]->image_url,
									$csgo_teams[$key]->$a[$i]->last_name,
									$csgo_teams[$key]->$a[$i]->name,
								]
							);
						}
					}
				}
			}*/
//		}

		return view('index');
	}

	/*public function getResultR6TabApi(Request $request) {
		dd($request->all());
		$nickname = $request->nickname;
	}*/

}
