<?php

namespace App\Http\Controllers;

use App\Team;
use App\Tournament;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Integer;
use function GuzzleHttp\Promise\all;


class TournamentController extends Controller
{
	public function getRequest() {
		$client = new Client(['headers' => ['content-type' => 'application/json', 'Accept' => 'application/json']]);

		$tableTeam = DB::table('teams')->get();

//		$parametrs = ['tournaments', 'teams'];

//		$header = get_headers("https://api.pandascore.co/csgo/tournaments?range[begin_at]=2018-01-01T17:00:00Z,2018-12-31T22:00:00Z&token=EYglI_LuOdfZ8rCzfYSn2_Z5C14GRPKIF9jl-vYjaLy-WB4XGJg");
		$header = get_headers("https://api.pandascore.co/csgo/teams?token=EYglI_LuOdfZ8rCzfYSn2_Z5C14GRPKIF9jl-vYjaLy-WB4XGJg");

		$str = preg_replace("/[^0-9]/", '', $header[15]);

		for ($page = 1; $page <= ceil($str / 50); $page++) {

			//Tournaments
			$response = $client->request('GET', 'https://api.pandascore.co/csgo/tournaments', [
				'query' => [
					'range[begin_at]' => '2018-01-01T17:00:00Z,2018-12-31T22:00:00Z',
					'token' => 'EYglI_LuOdfZ8rCzfYSn2_Z5C14GRPKIF9jl-vYjaLy-WB4XGJg'
				]
			]);

			$tournaments = $response->getBody();
			$tournaments = json_decode($tournaments);

			foreach ($tournaments as $key => $value) {

				DB::insert('insert into tournaments(
								tournament_id,
								league_id,
								league_img_url,
								league_name,
								prizepool,
								serie_full_name,
								serie_id,
								tournament_year,
								winner_id,
								begin_at,
								end_at,
								modified_at
							)values(?,?,?,?,?,?,?,?,?,?,?,?)',
					[
						$tournaments[$key]->id,
						$tournaments[$key]->league->id,
						$tournaments[$key]->league->image_url,
						$tournaments[$key]->league->name,
						$tournaments[$key]->prizepool,
						$tournaments[$key]->serie->full_name,
						$tournaments[$key]->serie->id,
						$tournaments[$key]->serie->year,
						$tournaments[$key]->winner_id,
						$tournaments[$key]->begin_at,
						$tournaments[$key]->end_at,
						$tournaments[$key]->modified_at,
					]
				);

				for ($i = 0; $i < count($tournaments[$key]->teams); $i++) {

					$tournament = Tournament::orderby('id', 'desc')->first();

					foreach ($tableTeam as $k => $item) {
						if ($item->team_id == $tournaments[$key]->teams[$i]->id) {
							$tournament->teams()->attach($item->id);
						}
					}
				}
			}
		}

		/*$tableTeam = DB::table('teams')->get();


		$tournament_id = Tournament::pluck('tournament_id')->toArray();

		$client = new Client(['headers' => ['content-type' => 'application/json', 'Accept' => 'application/json']]);


		//Tournaments
		$response = $client->request('GET', 'https://api.pandascore.co/csgo/tournaments', [
			'query' => [
				'token' => 'EYglI_LuOdfZ8rCzfYSn2_Z5C14GRPKIF9jl-vYjaLy-WB4XGJg'
			]
		]);

		$tournaments = $response->getBody();
		$tournaments = json_decode($tournaments);

		foreach ($tournaments as $key => $value) {
			for ($i = 0; $i < count($tournaments[$key]->teams); $i++) {
				foreach ($tournament_id as $item) {
					if($item != $tournaments[$key]->id) {
						$qry = DB::insert('insert into tournaments(
								tournament_id,
								league_id,
								league_img_url,
								league_name,
								prizepool,
								serie_full_name,
								serie_id,
								tournament_year,
								team_id,
								winner_id,
								begin_at,
								end_at,
								modified_at
							)values(?,?,?,?,?,?,?,?,?,?,?,?,?)',
							[
								$tournaments[$key]->id,
								$tournaments[$key]->league->id,
								$tournaments[$key]->league->image_url,
								$tournaments[$key]->league->name,
								$tournaments[$key]->prizepool,
								$tournaments[$key]->serie->full_name,
								$tournaments[$key]->serie->id,
								$tournaments[$key]->serie->year,
								$tournaments[$key]->teams[$i]->id,
								$tournaments[$key]->winner_id,
								$tournaments[$key]->begin_at,
								$tournaments[$key]->end_at,
								$tournaments[$key]->modified_at,
							]
						);

						$tournament = Tournament::orderby('id', 'desc')->first();

						foreach ($tableTeam as $k => $val) {
							if ($val->team_id == $tournaments[$key]->teams[$i]->id) {
								$tournament->teams()->attach($tournaments[$key]->teams[$i]->id);
							}
						}
						if($i+1 == count($tournaments[$key]->teams)) {
							var_dump($tournaments[$key]->teams);
							die();
						}
					}
				}
			}
		}*/

		/*$tournaments = Tournament::all()->where('tournament_id');

		dd($tournaments);*/

		/*return view('index');*/

	}

	public function index() {
//		$tournaments = Tournament::all();
//		$tournaments_grouped = $tournaments->groupBy('tournament_id');
		$tournaments = Tournament::paginate(10);
//		dd($tournaments_grouped);

		/*foreach ($tournaments as $tournament) {
			$tournament_name = str_replace(' ', '', $tournament->league_name);
			$myfile = fopen("$tournament_name.jpg", "w+")or die("Unable to open file!");
			fwrite($myfile, $tournament->league_img_url);
			fclose($myfile);

			dd($myfile);
		}*/

		$allTeams = DB::table('tournaments')->join('teams', 'tournaments');

//		dd($allTeams);
//		dd($tournaments);
		return view('pages.tournaments', compact('tournaments'));
	}

	public function show(Tournament $tournament)
	{
		return view('pages.tournament.show', compact('tournament'));
	}
}
