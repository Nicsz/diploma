<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->bigIncrements('id');
						$table->integer('match_id')->unsigned();
//						$table->foreign('match_id')->references('tournament_id')->on('tournaments');
						$table->string('match_name');
						$table->string('match_status');
						$table->boolean('draw');
						$table->integer('match_winner_id')->nullable();
						$table->string('begin_at');
						$table->string('end_at');
						$table->string('modified_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
