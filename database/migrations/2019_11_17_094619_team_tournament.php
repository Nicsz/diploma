<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TeamTournament extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_tournament', function (Blueprint $table) {
						$table->engine = 'MyISAM';
            $table->bigIncrements('id');
            $table->integer('team_id')->unsigned();
            $table->integer('tournament_id')->unsigned();
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
            $table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
				/*Schema::table('team_tournament', function (Blueprint $table) {
					$table->dropForeign('team_tournament_team_id_foreign');
					$table->dropForeign('team_tournament_tournament_id_foreign');
				});*/
				Schema::dropIfExists('team_tournament');
    }
}
