<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTournamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournaments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('tournament_id');
            $table->integer('league_id');
            $table->string('league_img_url');
            $table->string('league_name');
            $table->string('prizepool')->nullable();
            $table->string('serie_full_name');
            $table->integer('serie_id');
            $table->integer('tournament_year');
            $table->integer('team_id');
            $table->string('team_name');
            $table->integer('winner_id')->nullable();
						$table->string('begin_at');
						$table->string('end_at');
						$table->string('modified_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournaments');
    }
}
