# Cybersport analysis system on Laravel

With this web application you can get Rainbow Six Siege statistic players, teams, 
tournaments and matches. Statistic include:
## Information about players
<pre>
- Full name;
- Nickname;
- B-day;
- Hometown;
- Image.
</pre> 

## Information about teams
<pre>
- Name;
- Image.
</pre>

## Information about tournaments
<pre>
- League name;
- League image;
- Prizepool;
- Serie full name;
- Tournament full name;
- Tournament year;
- Teams participating in this tournament;
- Team which got win;
- Tournament start and end date.
</pre>

## Information about matches
<pre>
- Match name
- Status(will be, in progress, end);
- Team(s) which got win or draw;
- Match start and end date.
</pre>

## System Requirements

The following are required to function properly.

- Laravel 5.8
- PHP 7.3

## Getting Started

<pre>
git clone https://gitlab.com/Nicsz/diploma
<span class="pl-c1">cd</span> diploma
make .env file
php artisan migrate
php artisan db:seed
</pre>


## Run The Server

in your project root directory, run

``` shell

$ php artisan serve

```

## Configurable Options

The special endpoints in **web.php** file.


| Title             | Default              | Description                    |
| :---------------- | :------------------- | :----------------------------- |
| `playersList`     | `/players`           | The route get all players list |
| `teamsList`       | `/teams`             | The route get one teams list   |
| `tournamentsList` | `/tournaments`       | The route get one tournaments  |
