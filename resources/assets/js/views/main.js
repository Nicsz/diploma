$(function() {
    'use strict';

    $(document).on("click", ".platform", function() {
        if ($(this).hasClass("s_psn")) {
            setplatform('psn');
        }
        else if ($(this).hasClass("s_xbl")) {
            setplatform('xbl');
        }
        else {
            setplatform('uplay');
        }
    });

    function setplatform(platform) {
        $(".platform").removeClass("selected");
        if (platform == 'psn') {
            $(".s_psn").addClass("selected");
        } else if (platform == 'xbl') {
            $(".s_xbl").addClass("selected");
        } else {
            $(".s_uplay").addClass("selected");
        }
        // setCookie('platform', platform, '365');
    }

    $(document).ready(function(){
        $("#search").on("submit", function () {
            var hvalue = $('.selected').attr("data-name");
            $(this).append("<input type='hidden' name='name' value=' " + hvalue + " '/>");
           /* $(this).append("<input type='hidden' name='myname' value=' " + hvalue + " '/>");*/
        });
    });
});