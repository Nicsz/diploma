$(function() {
    'use strict';

    $('#body-row .collapse').collapse('hide');

// Collapse/Expand icon
    $('#collapse-icon').addClass('fa-angle-double-right');

// Collapse click
    $('[data-toggle=sidebar-colapse]').click(function() {
        SidebarCollapse();
    });

    function SidebarCollapse () {
        $('.menu-collapsed').toggleClass('d-none');
        $('.sidebar-submenu').toggleClass('d-none');
        $('.submenu-icon').toggleClass('d-none');
        $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');

        // Treating d-flex/d-none on separators with title
        var SeparatorTitle = $('.sidebar-separator-title');
        if ( SeparatorTitle.hasClass('d-flex') ) {
            SeparatorTitle.removeClass('d-flex');
        } else {
            SeparatorTitle.addClass('d-flex');
        }

        // Collapse/Expand icon
        $('#collapse-icon').toggleClass('fa-angle-double-right fa-angle-double-left');
    }

    $('[data-toggle=sidebar-colapse]').click(function() {
        $('#wrapper').toggleClass(' toggled');
        $(this).toggleClass('active');
    });

    $('#menu-toggle').click(function() {
        $('#wrapper').toggleClass('toggled');
        $(this).toggleClass('active');
    });

    $('.mob-sub-opener').click(function() {
        $(this).toggleClass('active');
        $(this).siblings('ul.sidebar-sub-nav').toggleClass('showit');
    });
});