<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
        @include('header')

        @include('pages.secondary.sidebar')
    {{--    <div class="row" id="body-row">--}}

        <div id="page-wrapper">
            <div id="global-container" class="container">
                @yield('content')
            </div>
        </div>

        <div class="bgwrapper"></div>
    {{--    </div>--}}
    {{--    <hr/>--}}
        {{--<div class="container" id="footerContainer">
            @include('footer')
        </div>--}}
    </div>
</body>
</html>
