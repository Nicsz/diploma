{{--
<div id="sidebar-container" class="sidebar-collapsed d-none d-md-block">

    <ul class="list-group">

        <li class="list-group-item sidebar-separator-title text-muted d-none align-items-center menu-collapsed">
            <small>MAIN MENU</small>
        </li>


        <a href="#submenu1" data-toggle="collapse" aria-expanded="false" class="bg-dark list-group-item list-group-item-action flex-column align-items-start collapsed">
            <div class="d-flex w-100 justify-content-start align-items-center">
                <span class="fa fa-dashboard fa-fw mr-3"></span>
                <span class="menu-collapsed d-none">Dashboard</span>
                <span class="submenu-icon ml-auto d-none"></span>
            </div>
        </a>

        <div id="submenu1" class="collapse sidebar-submenu d-none">
            <a href="#" class="list-group-item list-group-item-action bg-dark text-white">
                <span class="menu-collapsed">Charts</span>
            </a>
            <a href="#" class="list-group-item list-group-item-action bg-dark text-white">
                <span class="menu-collapsed">Reports</span>
            </a>
            <a href="#" class="list-group-item list-group-item-action bg-dark text-white">
                <span class="menu-collapsed">Tables</span>
            </a>
        </div>
        <a href="#submenu2" data-toggle="collapse" aria-expanded="false" class="bg-dark list-group-item list-group-item-action flex-column align-items-start collapsed">
            <div class="d-flex w-100 justify-content-start align-items-center">
                <span class="fa fa-user fa-fw mr-3"></span>
                <span class="menu-collapsed d-none">Profile</span>
                <span class="submenu-icon ml-auto d-none"></span>
            </div>
        </a>

        <div id="submenu2" class="collapse sidebar-submenu d-none">
            <a href="#" class="list-group-item list-group-item-action bg-dark text-white">
                <span class="menu-collapsed">Settings</span>
            </a>
            <a href="#" class="list-group-item list-group-item-action bg-dark text-white">
                <span class="menu-collapsed">Password</span>
            </a>
        </div>
        <a href="#" class="bg-dark list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-start align-items-center">
                <span class="fa fa-tasks fa-fw mr-3"></span>
                <span class="menu-collapsed d-none">Tasks</span>
            </div>
        </a>

        <li class="list-group-item sidebar-separator-title text-muted d-none align-items-center menu-collapsed">
            <small>OPTIONS</small>
        </li>

        <a href="#" class="bg-dark list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-start align-items-center">
                <span class="fa fa-calendar fa-fw mr-3"></span>
                <span class="menu-collapsed d-none">Calendar</span>
            </div>
        </a>
        <a href="#" class="bg-dark list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-start align-items-center">
                <span class="fa fa-envelope-o fa-fw mr-3"></span>
                <span class="menu-collapsed d-none">Messages <span class="badge badge-pill badge-primary ml-2">5</span></span>
            </div>
        </a>

        <li class="list-group-item sidebar-separator menu-collapsed d-none"></li>

        <a href="#" class="bg-dark list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-start align-items-center">
                <span class="fa fa-question fa-fw mr-3"></span>
                <span class="menu-collapsed d-none">Help</span>
            </div>
        </a>
        <a href="#" data-toggle="sidebar-colapse" class="bg-dark list-group-item list-group-item-action d-flex align-items-center active">
            <div class="d-flex w-100 justify-content-start align-items-center">
                <span id="collapse-icon" class="fa fa-2x mr-3 fa-angle-double-right"></span>
                <span id="collapse-text" class="menu-collapsed d-none">Collapse</span>
            </div>
        </a>

        <li class="list-group-item logo-separator d-flex justify-content-center">
            <img src="https://v4-alpha.getbootstrap.com/assets/brand/bootstrap-solid.svg" width="30" height="30">
        </li>
    </ul>
</div>
--}}

<div id="sidebar-wrapper">
    <div class="sm-wrap">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="/" title="Турниры по играм" class="logo"><img src="/html/img/logo_gt_footer.png" alt="game tournaments" class="png"></a>
            </li>
            {{--<li class="hasSubmenu">
                <a href="/dota-2" title="Турниры и матчи Dota 2" class="hvr-push"><span class="fa fa-home fa-fw mr-3"></span>
                    <span class="game-title">Dota 2</span>
                </a>
                <ul class="sidebar-sub-nav">
                    <li class="firsth"><a href="/dota-2" title="Турниры Dota 2">Dota 2</a></li>
                    <li><a href="/dota-2/matches" title="Dota 2 Матчи" class="tname">Матчи</a></li>
                    <li><a href="/dota-2/tournaments" title="Dota 2 " class="tname"></a></li>
                    <li><a href="/dota-2/team" title="Dota 2 Команды" class="tname">Команды</a></li>
                    <li><a href="/dota-2/player" title="Dota 2 Игроки" class="tname">Игроки</a></li>
                    <li><a href="/dota-2/bets" title="Dota 2 Ставки на матчи" class="tname">Ставки</a></li>
                    <li><a href="/dota-2/video" title="Dota 2 Записи игр / Видео матчей" class="tname">Видео</a></li>
                </ul>
            </li>--}}
            <li class="hasSubmenu">
                <a href="/csgo" title="Турниры и матчи CS:GO" class="hvr-push">
                    <img src="{{asset('/images/logo-704.png')}}" width="30" style="margin-bottom: 5px;">
                    <span class="game-title">CS:GO</span>
                </a>
                <ul class="sidebar-sub-nav">
                    <li class="firsth"><a href="/csgo" title="CS:GO Турниры">CS:GO</a></li>
                    <li><a href="/csgo/matches" title="CS:GO Матчи" class="tname">Турниры</a></li>
                    <li><a href="/csgo/tournaments" title="CS:GO " class="tname"></a></li>
                    <li><a href="/csgo/team" title="CS:GO Команды" class="tname">Команды</a></li>
                    <li><a href="/csgo/player" title="CS:GO Игроки" class="tname">Игроки</a></li>
                </ul>
            </li>
            {{--<li class="hasSubmenu">
                <a href="/hearthstone" title="Турниры и матчи HearthStone" class="hvr-push"><span class="fa fa-calendar fa-fw mr-3"></span>
                    <span class="game-title">HearthStone</span>
                </a>
                <ul class="sidebar-sub-nav">
                    <li class="firsth"><a href="/hearthstone" title="HearthStone Турниры">HearthStone</a></li>
                    <li><a href="/hearthstone/matches" title="HearthStone Матчи" class="tname">Матчи</a></li>
                    <li><a href="/hearthstone/tournaments" title="HearthStone " class="tname"></a></li>
                    <li><a href="/hearthstone/team" title="HearthStone Команды" class="tname">Команды</a></li>
                    <li><a href="/hearthstone/player" title="HearthStone Игроки" class="tname">Игроки</a></li>
                    <li><a href="/hearthstone/bets" title="HearthStone Ставки на матчи" class="tname">Ставки</a></li>
                    <li><a href="/hearthstone/video" title="HearthStone Записи игр / Видео матчей" class="tname">Видео</a></li>
                </ul>
            </li>--}}
            {{--<li class="hasSubmenu">
                <a href="/lol" title="Турниры и матчи LoL" class="hvr-push"><span class="fa fa-calendar fa-fw mr-3"></span>
                    <span class="game-title">LoL</span>
                </a>
                <ul class="sidebar-sub-nav">
                    <li class="firsth"><a href="/lol" title="HearthStone Турниры">LoL</a></li>
                    <li><a href="/lol/matches" title="HearthStone Матчи" class="tname">Матчи</a></li>
                    <li><a href="/lol/tournaments" title="HearthStone " class="tname"></a></li>
                    <li><a href="/lol/team" title="HearthStone Команды" class="tname">Команды</a></li>
                    <li><a href="/lol/player" title="HearthStone Игроки" class="tname">Игроки</a></li>
                    <li><a href="/lol/bets" title="HearthStone Ставки на матчи" class="tname">Ставки</a></li>
                    <li><a href="/lol/video" title="HearthStone Записи игр / Видео матчей" class="tname">Видео</a></li>
                </ul>
            </li>--}}
            {{--<li class="hasSubmenu">
                <a href="/overwatch" title="Турниры и матчи Overwatch" class="hvr-push"><span class="fa fa-calendar fa-fw mr-3"></span>
                    <span class="game-title">Overwatch</span>
                </a>
                <ul class="sidebar-sub-nav">
                    <li class="firsth"><a href="/overwatch" title="Overwatch Турниры">Overwatch</a></li>
                    <li><a href="/overwatch/matches" title="Overwatch Матчи" class="tname">Матчи</a></li>
                    <li><a href="/overwatch/tournaments" title="Overwatch " class="tname"></a></li>
                    <li><a href="/overwatch/team" title="Overwatch Команды" class="tname">Команды</a></li>
                    <li><a href="/overwatch/player" title="Overwatch Игроки" class="tname">Игроки</a></li>
                    <li><a href="/overwatch/bets" title="Overwatch Ставки на матчи" class="tname">Ставки</a></li>
                    <li><a href="/overwatch/video" title="Overwatch Записи игр / Видео матчей" class="tname">Видео</a></li>
                </ul>
            </li>--}}
        </ul>
    </div>
</div>
