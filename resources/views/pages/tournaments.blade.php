@extends('default')

@section('content')

	<img src="/public/PLGGrandSlam.jpg" alt="">
	
	<table class="table">
		<thead class="thead-dark">
			<tr>
				<th scope="col">Логотип</th>
				<th scope="col">Турнир</th>
				<th scope="col">Приз</th>
				<th scope="col">Победитель</th>
				<th scope="col">Дата начала</th>
				<th scope="col">Дата окончания</th>
				<th scope="col">Действие</th>
			</tr>
		</thead>
		<tbody>
			@foreach($tournaments as $tournament)
				<tr>
					<td><img src="{{$tournament->league_img_url}}" height="50" alt="tournament-image"></td>
					<td>{{$tournament->league_name}}</td>
					<td>{{$tournament->prizepool}}</td>
					<td>{{$tournament->winner_id}}</td>
					<td>{{Carbon\Carbon::parse($tournament->begin_at)->format('Y/m/d в H:i:s')}}</td>
					<td>{{Carbon\Carbon::parse($tournament->end_at)->format('Y/m/d в H:i:s')}}</td>
					<td><a href="/tournaments/{{ $tournament->id }}" class="btn btn-outline-dark">Осмотреть</a></td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{$tournaments->links()}}
@endsection