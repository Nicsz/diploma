@extends('default')

@section('content')

	{{--@php dd($_POST) @endphp--}}

	<div class="col-lg-12 smpadding">
		<div class="playerheader">
			<table width="100%">
				<tbody>
				<tr>
					<td>
						<table width="100%">
							<tbody>
							<tr>
								<td width="100"><div class="avatar"><img src="https://ubisoft-avatars.akamaized.net/{{$request->p_user}}/default_146_146.png"></div></td>
								<td class="playerinfo">
									<div class="name">{{$request->p_name}}</div>
									<div><u>#236387</u> Global<b></b>Level <u>{{$request->p_level}}</u><b></b>KD <u>{{$request->kd/100}}</u>&nbsp;&nbsp;<i class="fab fa-windows" style="color: #48A7E2;"></i></div>
									<div>
										<span id="favorite"><u class="cursor favit">Favorite <i class="far fa-star"></i></u></span><b></b>
										<u>196</u> Profile Visits
									</div>
									<div>
										Updated <u>18 hours</u> ago <b></b>&nbsp;<u><i class="fas fa-sync-alt cursor refreshplayer"></i></u><b></b>&nbsp;<a href="https://www.reddit.com/r/Tabwire/submit" target="_blank"><u>Report</u></a> </div>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-lg-4 smpadding">
		<div class="onlymob"></div>
		<div class="halfwidth">
			<div class="onlypc"></div>
			<div class="box">
				<div class="title">Current Season Rank</div>
				<div class="content currentseasonrank">
					<table>
						<tbody>
						<tr>
							<td class="currentrank"><img src="{{asset("storage/img/rank/10.svg")}}"></td>
							<td>
								<div class="ranktitle"><font color="#AB7C4F">Bronze 1</font></div>
								<div class="rankinfo">
									<s>{{$request->p_currentmmr}}</s> MMR in
									<s>
										@if($request->p_NA_currentmmr > 0)
											America
										@elseif($request->p_EU_currentmmr > 0)
											Europe
										@elseif($request->p_AS_currentmmr > 0)
											Asia
										@endif
									</s>
								</div>
							</td>
						</tr>
						</tbody>
					</table>
					<div class="liner"></div>
					<table width="100%">
						<tbody>
						<tr>
							<td>
								@if($request->p_NA_currentmmr > 0)
									America
								@elseif($request->p_EU_currentmmr > 0)
									Europe
								@elseif($request->p_AS_currentmmr > 0)
									Asia
								@endif
							</td>
							<td class="mmr"><s><font style="font-size: 11px;" color="green">+{{$request->p_currentmmr}}</font> {{$request->p_currentmmr}}</s> MMR</td></tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="onlypc"></div>
			<div class="box">
				<div class="title">TabRank <span class="right"><a href="/ranks" style="font-size: 12px; color: #CCCCCC;">Info <i class="fas fa-question-circle"></i></a></span></div>
				<div class="content currentseasonrank">
					<table>
						<tbody>
						<tr>
							<td width="60"><img style="margin-top: 4px;" src="/images/skill/5.png" width="45"></td>
							<td valign="top">
								<div class="ranktitle"><font color="#EEEEEE">Lieutenant</font></div>
								<div class="rankinfo">
									ELO <s>5,431</s> &nbsp; Rating <s>54.31</s>
								</div>
							</td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="halfwidth floatright">
			<div class="box">
				<div class="title">Past Seasons Top Rank</div>
				<div class="content smpadding">
					<div class="oneseason op_emberrise">
						<table>
							<tbody>
							<tr>
								<td><img src="{{asset("storage/img/rank/9.png")}}" width="40"></td><td>
									<div class="operationname">Ember Rise</div><div class="operationrank">Silver 4 <span class="sm">2254 MMR</span></div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="oneseason op_phantomsight">
						<table>
							<tbody>
							<tr>
								<td>
									<img src="{{asset("storage/img/rank/10.svg")}}" width="40">
								</td>
								<td>
									<div class="operationname">Phantom Sight</div><div class="operationrank">Silver 3 <span class="sm">2282 MMR</span></div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="oneseason op_burnthorizon">
						<table>
							<tbody>
							<tr>
								<td><img src="{{asset("storage/img/rank/0.svg")}}" width="40"></td><td>
									<div class="operationname">Burnt Horizon</div><div class="operationrank">Unranked <span class="sm">0 MMR</span></div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="oneseason op_windbastion">
						<table>
							<tbody>
							<tr>
								<td><img src="{{asset("storage/img/rank/0.svg")}}" width="40"></td><td>
									<div class="operationname">Wind Bastion</div><div class="operationrank">Unranked <span class="sm">0 MMR</span></div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="oneseason op_grimsky">
						<table>
							<tbody>
							<tr>
								<td><img src="{{asset("storage/img/rank/0.svg")}}" width="40"></td><td>
									<div class="operationname">Grim Sky</div><div class="operationrank">Unranked <span class="sm">0 MMR</span></div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="oneseason op_parabellum">
						<table>
							<tbody>
							<tr>
								<td><img src="{{asset("storage/img/rank/0.svg")}}" width="40"></td><td>
									<div class="operationname">Para Bellum</div><div class="operationrank">Unranked <span class="sm">0 MMR</span></div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="oneseason op_chimera">
						<table>
							<tbody>
							<tr>
								<td><img src="{{asset("storage/img/rank/0.svg")}}" width="40"></td><td>
									<div class="operationname">Chimera</div><div class="operationrank">Unranked <span class="sm">0 MMR</span></div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="oneseason op_whitenoise">
						<table>
							<tbody>
							<tr>
								<td><img src="{{asset("storage/img/rank/0.svg")}}" width="40"></td><td>
									<div class="operationname">White Noise</div><div class="operationrank">Unranked <span class="sm">0 MMR</span></div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="oneseason op_bloodorchid">
						<table>
							<tbody>
							<tr>
								<td><img src="{{asset("storage/img/rank/0.svg")}}" width="40"></td><td>
									<div class="operationname">Blood Orchid</div><div class="operationrank">Unranked <span class="sm">0 MMR</span></div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="oneseason op_health">
						<table>
							<tbody>
							<tr>
								<td><img src="{{asset("storage/img/rank/0.svg")}}" width="40"></td><td>
									<div class="operationname">Health</div><div class="operationrank">Unranked <span class="sm">0 MMR</span></div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="box onlypc">
				<div class="title">Alias History</div>
				<div class="content">
					<table width="100%">
						<tbody>
						<tr><td>Current</td><td class="right"><s>f-a-ll-e-n</s></td></tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@endsection