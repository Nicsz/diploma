{{--<h1 class="main-title">Hello, world!</h1>
<div class="test">
    <p>Some text</p>
</div>--}}
{{--<div class="row justify-content-center no-gutters">--}}
    {{--<div class="col-lg-12 smpadding">
        <div class="searchboxoutter">
            <div class="searchbox">
                <form id="search" action="{{route('show')}}" method="POST">
                    {!! csrf_field() !!}
                    --}}{{--<table>
                        <tbody>
                            <tr>
                                <td class="text">
                                    <input class="searchplayer" placeholder="Player name search..." type="search" name="nickname">
                                </td>
                                <td class="platforms">
                                    <i class="platform s_uplay fab fa-windows selected" data-name="uplay"></i>
                                    <i class="platform s_psn fab fa-playstation" data-name="psn"></i>
                                    <i class="platform s_xbl fab fa-xbox" data-name="xbl"></i>
                                </td>
                                <td class="go"><button type="submit"><i class="fas fa-angle-right"></i></button></td>
                            </tr>
                        </tbody>
                    </table>--}}{{--
                </form>
            </div>
        </div>
    </div>

    --}}{{--https://ubisoft-avatars.akamaized.net/02f9016c-173e-4f8d-9c91-054a6bdd689d/default_146_146.png--}}{{--

    <div class="col-lg-8 smpadding">
    </div>--}}

    {{--@foreach($request as $item)

    @endforeach--}}

<div class="main-page-banner">
    <div class="main-page-banner-top-row">
        Welcome to <span class="logotype">liquipedia</span>
    </div>
    <div class="main-page-banner-bottom-row">
        The Counter-Strike encyclopedia that <b>you</b> can edit
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-md-2 col-sm-4 col-xs-6">
        <div class="btn btn-outline-dark portal-pills" style="width:100%; margin-bottom: 10px;">
            <div class="panel-body"><a href="" title="Players"><span class="fas fa-user fa-4x"></span><br>Players</a></div>
        </div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
        <div class="btn btn-outline-dark portal-pills" style="width:100%; margin-bottom: 10px;">
            <div class="panel-body"><a href="" title="Teams"><span class="fad fa-users fa-4x"></span><br>Teams</a></div>
        </div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
        <div class="btn btn-outline-dark portal-pills" style="width:100%; margin-bottom: 10px;">
            <div class="panel-body"><a href="{{route('tournament')}}" title="Tournaments"><span class="fas fa-trophy fa-4x"></span><br>Tournaments</a></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 col-md-12 col-xl-6 pull-right-xl" style="order: 2;">
        <div class="panel panel-primary">
            <div class="panel-heading" style="border-bottom:0; font-size:160%"> Tournaments </div>
            <div>
                <ul class="tournaments-list">
                    <li><span class="tournaments-list-heading">Upcoming</span>
                        <ul class="tournaments-list-type-list">
                            <li>
                                <a href="/counterstrike/Intel_Extreme_Masters/Season_XIV/World_Championship">
                                    <span class="tournaments-list-name">
                                        <span class="league-icon-small-image">
                                            <img alt="Intel Extreme Masters" src="/commons/images/9/9f/ESL_2019_icon.png" title="Intel Extreme Masters" loading="lazy" width="50" height="50">
                                        </span>IEM XIV - World Championship
                                    </span>
                                    <small class="tournaments-list-dates">Feb 25 – Mar 01</small>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="tournaments-list-name">
                                        <span class="league-icon-small-image">
                                            <img alt="" src="/commons/images/8/8b/DH_Open_2018_Icon.png" loading="lazy" width="50" height="50">
                                        </span>DreamHack Anaheim 2020
                                    </span>
                                    <small class="tournaments-list-dates">Feb 21 – 23</small>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="tournaments-list-name">
                                        <span class="league-icon-small-image">
                                            <img alt="" src="/commons/images/2/21/BLAST_PREMIER_icon.png" loading="lazy" width="50" height="50">
                                        </span>
                                        BLAST Premier Spring 2020
                                    </span>
                                    <small class="tournaments-list-dates">Jan 31 – Feb 16</small>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="tournaments-list-name">
                                        <span class="league-icon-small-image">
                                            <img alt="" src="/commons/images/5/50/ICE_Challenge_2020_icon.png" loading="lazy" width="50" height="50">
                                        </span>
                                        ICE Challenge 2020
                                    </span>
                                    <small class="tournaments-list-dates">Feb 01 – 06</small>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="tournaments-list-name">
                                        <span class="league-icon-small-image">
                                            <img alt="" src="/commons/images/8/8b/DH_Open_2018_Icon.png" loading="lazy" width="50" height="50">
                                        </span>
                                        DreamHack Leipzig 2020
                                    </span>
                                    <small class="tournaments-list-dates">Jan 24 – 26</small>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <span class="tournaments-list-heading">Completed</span>
                        <ul class="tournaments-list-type-list">
                            <li>
                                <a href="">
                                    <span class="tournaments-list-name">
                                        <span class="league-icon-small-image">
                                            <img alt="" src="/commons/images/a/ac/Champions_Cup_icon.png" loading="lazy" width="50" height="50">
                                        </span>
                                        Champions Cup Finals
                                    </span>
                                    <small class="tournaments-list-dates">Dec 19 – 22</small>
                                </a>
                            </li>
                            <li>
                                <a href="/counterstrike/EPICENTER/2019">
                                    <span class="tournaments-list-name">
                                        <span class="league-icon-small-image">
                                            <img alt="" src="/commons/images/2/29/Epicenter_small.png" loading="lazy" width="50" height="50">
                                        </span>
                                        EPICENTER 2019
                                    </span>
                                    <small class="tournaments-list-dates">Dec 17 – 22</small>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="tournaments-list-name">
                                        <span class="league-icon-small-image">
                                            <img alt="" src="/commons/images/7/72/Beyond_The_Summit_2018_icon.png" loading="lazy" width="50" height="50">
                                        </span>
                                        cs_summit 5
                                    </span>
                                    <small class="tournaments-list-dates">Dec 12 – 15</small>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="tournaments-list-name">
                                        <span class="league-icon-small-image">
                                            <img alt="" src="/commons/images/0/0e/Esea.png" loading="lazy" width="50" height="50">
                                        </span>
                                        ESEA S32 - Global Challenge
                                    </span>
                                    <small class="tournaments-list-dates">Dec 14 – 15</small>
                                </a>
                            </li>
                            <li>
                                <a href="/counterstrike/DreamHack/2019/Sevilla">
                                    <span class="tournaments-list-name">
                                        <span class="league-icon-small-image">
                                            <img alt="" src="/commons/images/8/8b/DH_Open_2018_Icon.png" loading="lazy" width="50" height="50">
                                        </span>
                                        DH Open Sevilla 2019
                                    </span>
                                    <small class="tournaments-list-dates">Dec 13 – 15</small>
                                </a>
                            </li>
                            <li>
                                <a href="/counterstrike/BLAST_Pro_Series/2019/Global_Final">
                                    <span class="tournaments-list-name">
                                        <span class="league-icon-small-image">
                                            <img alt="" src="/commons/images/f/f3/BLAST_Pro_Series_icon.png" loading="lazy" width="50" height="50">
                                        </span>
                                        BLAST Pro Series: Global Final
                                    </span>
                                    <small class="tournaments-list-dates">Dec 12 – 14</small>
                                </a>
                            </li>
                            <li>
                                <a href="/counterstrike/ESL/Pro_League/Season_10/Finals">
                                    <span class="tournaments-list-name">
                                        <span class="league-icon-small-image">
                                            <img alt="ESL Pro League" src="/commons/images/1/10/ESL_Pro_League_2019_new_icon.png" title="ESL Pro League" loading="lazy" width="50" height="50">
                                        </span>
                                        EPL Season 10 - Finals
                                    </span>
                                    <small class="tournaments-list-dates">Dec 03 – 08</small>
                                </a>
                            </li>
                            <li>
                                <a href="/counterstrike/Esports_Championship_Series/Season_8">
                                    <span class="tournaments-list-name">
                                        <span class="league-icon-small-image">
                                            <img alt="" src="/commons/images/b/bb/Esports_Championship_Series_2019_icon.png" loading="lazy" width="50" height="50">
                                        </span>
                                        ECS Season 8 - Finals
                                    </span>
                                    <small class="tournaments-list-dates">Nov 28 – Dec 01</small>
                                </a>
                            </li>
                            <li>
                                <a href="/counterstrike/DreamHack/2019/Winter">
                                    <span class="tournaments-list-name">
                                        <span class="league-icon-small-image">
                                            <img alt="" src="/commons/images/8/8b/DH_Open_2018_Icon.png" loading="lazy" width="50" height="50">
                                        </span>
                                        DH Open Winter 2019
                                    </span>
                                    <small class="tournaments-list-dates">Nov 29 – Dec 01</small>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div style="margin-left:calc(50% - 150px); margin-bottom:10px;"><div class="navigation-not-searchable"><div id="300x250_BTF"></div></div></div>
    </div>
    <div class="col-sm-6 col-md-12 col-xl-6" style="order: 1;">
        <div class="panel panel-primary toggle-area toggle-area-1" data-toggle-area="1">
            <div class="panel-heading" style="border-bottom:0">
                <span style="font-size:160%">
                    <span data-toggle-area-content="1">
                        <span class="mobile-hide&quot;">Upcoming </span>Matches
                    </span>
                </span>
            </div>
            <div>
                <div data-toggle-area-content="1">
                    <table class="table table-striped infobox_matches_content">
                        <tbody>
                            <tr>
                                <td class="team-left"><span data-highlightingclass="Heroic" class="team-template-team2-short"><span class="team-template-text"><a href="/counterstrike/Heroic" title="Heroic">Heroic</a></span> <span class="team-template-image"><a href="/counterstrike/Heroic" title="Heroic"><img alt="" src="/commons/images/1/1e/Heroic_2019_std.png" loading="lazy" width="120" height="50"></a></span></span></td>
                                <td class="versus">2:0</td>
                                <td class="team-right"><span data-highlightingclass="Phoenix" class="team-template-team-short"><span class="team-template-image"><a href="/counterstrike/Phoenix" title="Phoenix"><img alt="" src="/commons/images/f/f6/Phoenix_Esports_std.png" loading="lazy" width="120" height="50"></a></span> <span class="team-template-text"><a href="/counterstrike/Phoenix" title="Phoenix">Phoenix</a></span></span></td>
                            </tr>
                            <tr>
                                <td class="match-filler" colspan="3"><span class="match-countdown"><span class="timer-object timer-object-countdown-only" data-stream-twitch="StarLadder - CS (English)" data-separator="&amp;#8203;" data-timestamp="1578243600"><span class="timer-object-date">January 5, 2020 - 19:00 <abbr data-tz="+2:00" title="Eastern European Standard Time (UTC+2)">EET</abbr></span><span class="timer-object-separator">&amp;#8203;</span><span class="timer-object-countdown"><span class="timer-object-countdown-time"><span class="timer-object-countdown-live">LIVE!</span></span> - <a href="/counterstrike/Special:Stream/twitch/StarLadder_-_CS_(English)"><i class="lp-icon lp-icon-21 lp-twitch"></i></a></span></span></span><div style="min-width:175px; max-width:185px; float:right; white-space:nowrap;"><span class="league-icon-small-image" style="float:right; vertical-align:middle;"><a href="/counterstrike/GG.Bet_Winter_Cup" title="GG.Bet Winter Cup"><img alt="GG.Bet Winter Cup" src="/commons/images/9/9f/GG.Bet_Winter_Cup_icon.png" loading="lazy" width="50" height="50"></a></span><div style="overflow:hidden; text-overflow:ellipsis; max-width: 170px; vertical-align:middle; white-space:nowrap; font-size:11px; height:16px; margin-top:3px;"><a href="/counterstrike/GG.Bet_Winter_Cup" title="GG.Bet Winter Cup">GG.Bet Winter Cup</a>&nbsp;</div></div></td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-striped infobox_matches_content">
                        <tbody>
                            <tr>
                                <td class="team-left"><span data-highlightingclass="TBD" class="team-template-team-short"><span class="team-template-text"><abbr title="To Be Determined">TBD</abbr></span> <span class="team-template-image"><img alt="" src="/commons/images/9/93/Logo_filler_std.png" loading="lazy" width="60" height="25"></span></span></td>
                                <td class="versus">vs</td>
                                <td class="team-right"><span data-highlightingclass="TBD" class="team-template-team-short"><span class="team-template-image"><img alt="" src="/commons/images/9/93/Logo_filler_std.png" loading="lazy" width="60" height="25"></span> <span class="team-template-text"><abbr title="To Be Determined">TBD</abbr></span></span></td>
                            </tr>
                            <tr>
                                <td class="match-filler" colspan="3"><span class="match-countdown"><span class="timer-object timer-object-countdown-only" data-separator="&amp;#8203;" data-timestamp="1578402000"><span class="timer-object-date">January 7, 2020 - 15:00 <abbr data-tz="+2:00" title="Eastern European Standard Time (UTC+2)">EET</abbr></span><span class="timer-object-separator">&amp;#8203;</span><span class="timer-object-countdown"><span class="timer-object-countdown-time">1d 15h 31m</span></span></span></span><div style="min-width:175px; max-width:185px; float:right; white-space:nowrap;"><span class="league-icon-small-image" style="float:right; vertical-align:middle;"><a href="/counterstrike/Intel_Extreme_Masters/Season_XIV/World_Championship/Asia" title="IEM Katowice '20: Asia Qual"><img alt="IEM Katowice '20: Asia Qual" src="/commons/images/9/9f/ESL_2019_icon.png" loading="lazy" width="50" height="50"></a></span><div style="overflow:hidden; text-overflow:ellipsis; max-width: 170px; vertical-align:middle; white-space:nowrap; font-size:11px; height:16px; margin-top:3px;"><a href="/counterstrike/Intel_Extreme_Masters/Season_XIV/World_Championship/Asia" title="Intel Extreme Masters/Season XIV/World Championship/Asia">IEM Katowice '20: Asia Qual</a>&nbsp;</div></div></td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-striped infobox_matches_content">
                        <tbody>
                        <tr>
                            <td class="team-left"><span data-highlightingclass="TBD" class="team-template-team-short"><span class="team-template-text"><abbr title="To Be Determined">TBD</abbr></span> <span class="team-template-image"><img alt="" src="/commons/images/9/93/Logo_filler_std.png" loading="lazy" width="60" height="25"></span></span></td>
                            <td class="versus">vs</td>
                            <td class="team-right"><span data-highlightingclass="TBD" class="team-template-team-short"><span class="team-template-image"><img alt="" src="/commons/images/9/93/Logo_filler_std.png" loading="lazy" width="60" height="25"></span> <span class="team-template-text"><abbr title="To Be Determined">TBD</abbr></span></span></td>
                        </tr>
                        <tr>
                            <td class="match-filler" colspan="3"><span class="match-countdown"><span class="timer-object timer-object-countdown-only" data-separator="&amp;#8203;" data-timestamp="1578438000"><span class="timer-object-date">January 8, 2020 - 01:00 <abbr data-tz="+2:00" title="Eastern European Standard Time (UTC+2)">EET</abbr></span><span class="timer-object-separator">&amp;#8203;</span><span class="timer-object-countdown"><span class="timer-object-countdown-time">2d 1h 31m</span></span></span></span><div style="min-width:175px; max-width:185px; float:right; white-space:nowrap;"><span class="league-icon-small-image" style="float:right; vertical-align:middle;"><a href="/counterstrike/Intel_Extreme_Masters/Season_XIV/World_Championship/North_America" title="IEM Katowice '20: NA Qual"><img alt="IEM Katowice '20: NA Qual" src="/commons/images/9/9f/ESL_2019_icon.png" loading="lazy" width="50" height="50"></a></span><div style="overflow:hidden; text-overflow:ellipsis; max-width: 170px; vertical-align:middle; white-space:nowrap; font-size:11px; height:16px; margin-top:3px;"><a href="/counterstrike/Intel_Extreme_Masters/Season_XIV/World_Championship/North_America" title="Intel Extreme Masters/Season XIV/World Championship/North America">IEM Katowice '20: NA Qual</a>&nbsp;</div></div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div data-toggle-area-content="2"><br><center>There are currently no upcoming featured matches</center><br></div>
                <div data-toggle-area-content="3">

                    <table class="table table-striped infobox_matches_content" style="background-image:linear-gradient(to right, rgb(251, 223, 223) 25%, rgb(255, 255, 255) 45%, rgb(255, 255, 255) 55%, rgb(221, 244, 221) 75%);">
                        <tbody>
                            <tr>
                                <td style="" class="team-left"><span data-highlightingclass="Gambit Youngsters" class="team-template-team2-short"><span class="team-template-text"><a href="/counterstrike/Gambit_Youngsters" title="Gambit Youngsters">GambitY</a></span> <span class="team-template-image"><a href="/counterstrike/Gambit_Youngsters" title="Gambit Youngsters"><img alt="" src="/commons/images/0/05/Gambit_newlogo_std.png" loading="lazy" width="120" height="50"></a></span></span></td>
                                <td class="versus">0:<b>2</b></td>
                                <td style="font-weight:bold;" class="team-right"><span data-highlightingclass="Phoenix" class="team-template-team-short"><span class="team-template-image"><a href="/counterstrike/Phoenix" title="Phoenix"><img alt="" src="/commons/images/f/f6/Phoenix_Esports_std.png" loading="lazy" width="120" height="50"></a></span> <span class="team-template-text"><a href="/counterstrike/Phoenix" title="Phoenix">Phoenix</a></span></span></td>
                            </tr>
                            <tr>
                                <td class="match-filler" colspan="3"><span class="match-countdown" style="font-size:11px"><span class="timer-object timer-object-datetime-only" data-separator="&amp;#8203;" data-timestamp="1578232800"><span class="timer-object-date">January 5, 2020 - 16:00 <abbr data-tz="+2:00" title="Eastern European Standard Time (UTC+2)">EET</abbr></span><span class="timer-object-separator">&amp;#8203;</span><span class="timer-object-countdown"><span class="timer-object-countdown-time"><span class="timer-object-countdown-live">LIVE!</span></span></span></span>&nbsp;&nbsp;</span><div style="min-width:175px; max-width:185px; float:right; white-space:nowrap;"><span style="float:right"><span class="league-icon-small-image"><a href="/counterstrike/GG.Bet_Winter_Cup" title="GG.Bet Winter Cup"><img alt="GG.Bet Winter Cup" src="/commons/images/9/9f/GG.Bet_Winter_Cup_icon.png" loading="lazy" width="50" height="50"></a></span></span><div style="overflow:hidden; text-overflow:ellipsis; max-width: 170px; vertical-align:middle; white-space:nowrap; font-size:11px; height:16px; margin-top:3px;"><a href="/counterstrike/GG.Bet_Winter_Cup" title="GG.Bet Winter Cup">GG.Bet Winter Cup</a>&nbsp;</div></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-striped infobox_matches_content" style="background-image:linear-gradient(to left, rgb(251, 223, 223) 25%, rgb(255, 255, 255) 45%, rgb(255, 255, 255) 55%, rgb(221, 244, 221) 75%);">
                        <tbody>
                            <tr>
                                <td style="font-weight:bold;" class="team-left"><span data-highlightingclass="Global Esports" class="team-template-team2-short"><span class="team-template-text"><a href="/counterstrike/Global_Esports" title="Global Esports">Global</a></span> <span class="team-template-image"><a href="/counterstrike/Global_Esports" title="Global Esports"><img alt="" src="/commons/images/2/26/GE_-_Global_Esports_logo_std.png" loading="lazy" width="120" height="50"></a></span></span></td>
                                <td class="versus"><b>16</b>:7</td>
                                <td style="" class="team-right"><span data-highlightingclass="BL4ZE Esports" class="team-template-team-short"><span class="team-template-image"><a href="/counterstrike/BL4ZE_Esports" title="BL4ZE Esports"><img alt="" src="/commons/images/b/b6/BL4ZE_Esports_std.png" loading="lazy" width="120" height="50"></a></span> <span class="team-template-text"><a href="/counterstrike/index.php?title=BL4ZE_Esports&amp;action=edit&amp;redlink=1" class="new" title="BL4ZE Esports (page does not exist)">BL4ZE</a></span></span></td>
                            </tr>
                            <tr>
                                <td class="match-filler" colspan="3"><span class="match-countdown" style="font-size:11px"><span class="timer-object timer-object-datetime-only" data-separator="&amp;#8203;" data-timestamp="1578227400"><span class="timer-object-date">January 5, 2020 - 14:30 <abbr data-tz="+2:00" title="Eastern European Standard Time (UTC+2)">EET</abbr></span><span class="timer-object-separator">&amp;#8203;</span><span class="timer-object-countdown"><span class="timer-object-countdown-time"><span class="timer-object-countdown-live">LIVE!</span></span></span></span>&nbsp;&nbsp;</span><div style="min-width:175px; max-width:185px; float:right; white-space:nowrap;"><span style="float:right"><span class="league-icon-small-image"><a href="/counterstrike/ESL/India_Premiership/2019/Winter/Phase_2" title="ESL India Premiership Winter 2019 - Masters League Phase 2"><img alt="ESL India Premiership Winter 2019 - Masters League Phase 2" src="/commons/images/4/49/ESL_India_Premiership_2019_icon.png" loading="lazy" width="50" height="50"></a></span></span><div style="overflow:hidden; text-overflow:ellipsis; max-width: 170px; vertical-align:middle; white-space:nowrap; font-size:11px; height:16px; margin-top:3px;"><a href="/counterstrike/ESL/India_Premiership/2019/Winter/Phase_2" title="ESL/India Premiership/2019/Winter/Phase 2">India Premiership: Winter '19</a>&nbsp;</div></div></td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-striped infobox_matches_content" style="background-image:linear-gradient(to right, rgb(251, 223, 223) 25%, rgb(255, 255, 255) 45%, rgb(255, 255, 255) 55%, rgb(221, 244, 221) 75%);">
                        <tbody>
                            <tr>
                                <td style="" class="team-left"><span data-highlightingclass="Wicked Gaming (Indian team)" class="team-template-team2-short"><span class="team-template-text"><a href="/counterstrike/index.php?title=Wicked_Gaming_(Indian_team)&amp;action=edit&amp;redlink=1" class="new" title="Wicked Gaming (Indian team) (page does not exist)">Wicked</a></span> <span class="team-template-image"><a href="/counterstrike/Wicked_Gaming_(Indian_team)" title="Wicked Gaming (Indian team)"><img alt="" src="/commons/images/6/63/Csgologo_std.png" loading="lazy" width="120" height="50"></a></span></span></td>
                                <td class="versus">5:<b>16</b></td>
                                <td style="font-weight:bold;" class="team-right"><span data-highlightingclass="Entity Gaming" class="team-template-team-short"><span class="team-template-image"><a href="/counterstrike/Entity_Gaming" title="Entity Gaming"><img alt="" src="/commons/images/1/1d/Entity_2017_std.png" loading="lazy" width="120" height="50"></a></span> <span class="team-template-text"><a href="/counterstrike/Entity_Gaming" title="Entity Gaming">Entity</a></span></span></td>
                            </tr>
                            <tr>
                                <td class="match-filler" colspan="3"><span class="match-countdown" style="font-size:11px"><span class="timer-object timer-object-datetime-only" data-separator="&amp;#8203;" data-timestamp="1578223800"><span class="timer-object-date">January 5, 2020 - 13:30 <abbr data-tz="+2:00" title="Eastern European Standard Time (UTC+2)">EET</abbr></span><span class="timer-object-separator">&amp;#8203;</span><span class="timer-object-countdown"><span class="timer-object-countdown-time"><span class="timer-object-countdown-live">LIVE!</span></span></span></span>&nbsp;&nbsp;</span><div style="min-width:175px; max-width:185px; float:right; white-space:nowrap;"><span style="float:right"><span class="league-icon-small-image"><a href="/counterstrike/ESL/India_Premiership/2019/Winter/Phase_2" title="ESL India Premiership Winter 2019 - Masters League Phase 2"><img alt="ESL India Premiership Winter 2019 - Masters League Phase 2" src="/commons/images/4/49/ESL_India_Premiership_2019_icon.png" loading="lazy" width="50" height="50"></a></span></span><div style="overflow:hidden; text-overflow:ellipsis; max-width: 170px; vertical-align:middle; white-space:nowrap; font-size:11px; height:16px; margin-top:3px;"><a href="/counterstrike/ESL/India_Premiership/2019/Winter/Phase_2" title="ESL/India Premiership/2019/Winter/Phase 2">India Premiership: Winter '19</a>&nbsp;</div></div></td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-striped infobox_matches_content" style="background-image:linear-gradient(to left, rgb(251, 223, 223) 25%, rgb(255, 255, 255) 45%, rgb(255, 255, 255) 55%, rgb(221, 244, 221) 75%);">
                        <tbody>
                            <tr>
                                <td style="font-weight:bold;" class="team-left"><span data-highlightingclass="eUnited" class="team-template-team2-short"><span class="team-template-text"><a href="/counterstrike/EUnited" title="EUnited">eU</a></span> <span class="team-template-image"><a href="/counterstrike/EUnited" title="EUnited"><img alt="" src="/commons/images/b/b7/EUnitedlogo_std.png" loading="lazy" width="120" height="50"></a></span></span></td>
                                <td class="versus"><b>2</b>:0</td>
                                <td style="" class="team-right"><span data-highlightingclass="Rap Gang" class="team-template-team-short"><span class="team-template-image"><a href="/counterstrike/Rap_Gang" title="Rap Gang"><img alt="" src="/commons/images/6/63/Csgologo_std.png" loading="lazy" width="120" height="50"></a></span> <span class="team-template-text"><a href="/counterstrike/index.php?title=Rap_Gang&amp;action=edit&amp;redlink=1" class="new" title="Rap Gang (page does not exist)">Rap Gang</a></span></span></td>
                            </tr>
                            <tr>
                                <td class="match-filler" colspan="3"><span class="match-countdown" style="font-size:11px"><span class="timer-object timer-object-datetime-only" data-separator="&amp;#8203;" data-timestamp="1578187800"><span class="timer-object-date">January 5, 2020 - 03:30 <abbr data-tz="+2:00" title="Eastern European Standard Time (UTC+2)">EET</abbr></span><span class="timer-object-separator">&amp;#8203;</span><span class="timer-object-countdown"><span class="timer-object-countdown-time"></span></span></span>&nbsp;&nbsp;</span><div style="min-width:175px; max-width:185px; float:right; white-space:nowrap;"><span style="float:right"><span class="league-icon-small-image"><a href="/counterstrike/Mythic_Cup/5" title="Mythic Cup #5"><img alt="Mythic Cup #5" src="/commons/images/6/63/Mythic_icon.png" loading="lazy" width="50" height="50"></a></span></span><div style="overflow:hidden; text-overflow:ellipsis; max-width: 170px; vertical-align:middle; white-space:nowrap; font-size:11px; height:16px; margin-top:3px;"><a href="/counterstrike/Mythic_Cup/5" title="Mythic Cup/5">Mythic Cup #5</a>&nbsp;</div></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-striped infobox_matches_content" style="background-image:linear-gradient(to right, rgb(251, 223, 223) 25%, rgb(255, 255, 255) 45%, rgb(255, 255, 255) 55%, rgb(221, 244, 221) 75%);">
                        <tbody>
                            <tr>
                                <td style="" class="team-left"><span data-highlightingclass="Bushido Boyz" class="team-template-team2-short"><span class="team-template-text"><a href="/counterstrike/index.php?title=Bushido_Boyz&amp;action=edit&amp;redlink=1" class="new" title="Bushido Boyz (page does not exist)">Boyz</a></span> <span class="team-template-image"><a href="/counterstrike/Bushido_Boyz" title="Bushido Boyz"><img alt="" src="/commons/images/c/c9/Bushido_boyz_std.png" loading="lazy" width="120" height="50"></a></span></span></td>
                                <td class="versus">1:<b>2</b></td>
                                <td style="font-weight:bold;" class="team-right"><span data-highlightingclass="Rap Gang" class="team-template-team-short"><span class="team-template-image"><a href="/counterstrike/Rap_Gang" title="Rap Gang"><img alt="" src="/commons/images/6/63/Csgologo_std.png" loading="lazy" width="120" height="50"></a></span> <span class="team-template-text"><a href="/counterstrike/index.php?title=Rap_Gang&amp;action=edit&amp;redlink=1" class="new" title="Rap Gang (page does not exist)">Rap Gang</a></span></span></td>
                            </tr>
                            <tr>
                                <td class="match-filler" colspan="3"><span class="match-countdown" style="font-size:11px"><span class="timer-object timer-object-datetime-only" data-separator="&amp;#8203;" data-timestamp="1578175800"><span class="timer-object-date">January 5, 2020 - 00:10 <abbr data-tz="+2:00" title="Eastern European Standard Time (UTC+2)">EET</abbr></span><span class="timer-object-separator">&amp;#8203;</span><span class="timer-object-countdown"><span class="timer-object-countdown-time"></span></span></span>&nbsp;&nbsp;</span><div style="min-width:175px; max-width:185px; float:right; white-space:nowrap;"><span style="float:right"><span class="league-icon-small-image"><a href="/counterstrike/Mythic_Cup/5" title="Mythic Cup #5"><img alt="Mythic Cup #5" src="/commons/images/6/63/Mythic_icon.png" loading="lazy" width="50" height="50"></a></span></span><div style="overflow:hidden; text-overflow:ellipsis; max-width: 170px; vertical-align:middle; white-space:nowrap; font-size:11px; height:16px; margin-top:3px;"><a href="/counterstrike/Mythic_Cup/5" title="Mythic Cup/5">Mythic Cup #5</a>&nbsp;</div></div></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div style="text-align:center; padding:5px"><i><a href="/counterstrike/Liquipedia:Matches" title="Liquipedia:Matches">See more matches</a></i></div>
            </div>
        </div>
    </div>
</div>