<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** @var  \Illuminate\Routing\Router $router */

Route::get('/input', function () {
    return view('apiRequest');
});

/*$router->get('/', function () {
	$tournament = \App\Tournament::first();

	dd($tournament);

	$teams = \App\Team::first();

	$tournament->teams()->attach($teams);

	dd($teams);
});*/

$router->get('/', 'TournamentController@getRequest')->name('get');
/*$router->get('/', 'IndexController@index')->name('main');
$router->get('/players', 'PlayerController@index')->name('player');
$router->get('/tournaments', 'TournamentController@index')->name('tournament');
$router->get('/tournaments/{tournament}', "TournamentController@show");*/
/*$router->get('/create', 'TeamController@create')->name('create');*/


//$router->post('/', 'ApiRequestController@insert');
/*$router->get('/result', 'UserstatController@insertform')->name('result');
$router->post('/result', 'UserstatController@postShow')->name('show');*/

//Test
$router->get('/devices','DevicesController@index');
$router->get('/devices/create','DevicesController@create');
$router->post('/devicesaction','DevicesController@storeDevice');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');